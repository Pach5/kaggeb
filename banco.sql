CREATE TABLE autor
(
  id            NUMBER   NOT NULL,
  nome          VARCHAR2 NOT NULL,
  nacionalidade VARCHAR2 NOT NULL,
  data_cadastro DATETIME DEFAULT SYSDATE NOT NULL,
  CONSTRAINT PK_autor PRIMARY KEY (id)
);

CREATE TABLE autor_item
(
  item_id  NUMBER NOT NULL,
  autor_id NUMBER NOT NULL,
  CONSTRAINT PK_autor_item PRIMARY KEY (item_id, autor_id)
);

CREATE TABLE conversa
(
  id              NUMBER   NOT NULL,
  remetente_id    NUMBER   NOT NULL,
  destinatario_id NUMBER   NOT NULL,
  status_id       NUMBER   NOT NULL,
  data            DATETIME DEFAULT SYSDATE NOT NULL,
  CONSTRAINT PK_conversa PRIMARY KEY (id)
);

CREATE TABLE emprestimo
(
  id              NUMBER   NOT NULL,
  destinatario_id NUMBER   NOT NULL,
  item_id         NUMBER   NOT NULL,
  data_retirada   DATETIME NOT NULL,
  data_devolucao  DATETIME NOT NULL,
  CONSTRAINT PK_emprestimo PRIMARY KEY (id)
);

CREATE TABLE endereco
(
  id     NUMBER   NOT NULL,
  cidade VARCHAR2 NOT NULL,
  bairro VARCHAR2 NOT NULL,
  rua    VARCHAR2 NOT NULL,
  numero VARCHAR2 NOT NULL,
  CONSTRAINT PK_endereco PRIMARY KEY (id)
);

CREATE TABLE genero
(
  id     NUMBER   NOT NULL,
  genero VARCHAR2 NOT NULL,
  CONSTRAINT PK_genero PRIMARY KEY (id)
);

CREATE TABLE item
(
  id              NUMBER   NOT NULL,
  titulo          VARCHAR2 NOT NULL,
  tipo            VARCHAR2 NOT NULL,
  idioma          VARCHAR2 NOT NULL,
  genero_id       NUMBER   NOT NULL,
  proprietario_id NUMBER   NOT NULL,
  data_cadastro   DATETIME DEFAULT SYSDATE NOT NULL,
  CONSTRAINT PK_item PRIMARY KEY (id)
);

CREATE TABLE mensagem
(
  id           NUMBER   NOT NULL,
  mensagem     VARCHAR2 NOT NULL,
  conversa_id  NUMBER   NOT NULL,
  remetente_id NUMBER   NOT NULL,
  status_id       NUMBER   NOT NULL,
  data         DATETIME DEFAULT SYSDATE NOT NULL,
  CONSTRAINT PK_mensagem PRIMARY KEY (id)
);

CREATE TABLE status_conversa
(
  id     NUMBER   NOT NULL,
  status VARCHAR2 NOT NULL,
  CONSTRAINT PK_status_conversa PRIMARY KEY (id)
);

CREATE TABLE status_mensagem
(
  id     NUMBER   NOT NULL,
  status VARCHAR2 NOT NULL,
  CONSTRAINT PK_status_mensagem PRIMARY KEY (id)
);

CREATE TABLE troca
(
  data_troca  DATETIME DEFAULT SYSDATE NOT NULL,
  id          NUMBER   NOT NULL,
  usuario1_id NUMBER   NOT NULL,
  usuario2_id NUMBER   NOT NULL,
  item1_id    NUMBER   NOT NULL,
  item2_id    NUMBER   NOT NULL,
  CONSTRAINT PK_troca PRIMARY KEY (id)
);

CREATE TABLE usuario
(
  id            NUMBER   NOT NULL,
  nome          VARCHAR2 NOT NULL,
  telefone      VARCHAR2 NOT NULL,
  email         VARCHAR2 NOT NULL,
  senha         VARCHAR2 NOT NULL,
  cpf           VARCHAR2 NOT NULL,
  endereco_id   NUMBER   NOT NULL,
  data_cadastro DATETIME DEFAULT SYSDATE NOT NULL,
  is_active     NUMBER   DEFAULT 1 NOT NULL,
  CONSTRAINT PK_usuario PRIMARY KEY (id)
);

ALTER TABLE usuario
  ADD CONSTRAINT FK_endereco_TO_usuario
    FOREIGN KEY (endereco_id)
    REFERENCES endereco (id);

ALTER TABLE item
  ADD CONSTRAINT FK_genero_TO_item
    FOREIGN KEY (genero_id)
    REFERENCES genero (id);

ALTER TABLE emprestimo
  ADD CONSTRAINT FK_usuario_TO_emprestimo
    FOREIGN KEY (destinatario_id)
    REFERENCES usuario (id);

ALTER TABLE emprestimo
  ADD CONSTRAINT FK_item_TO_emprestimo
    FOREIGN KEY (item_id)
    REFERENCES item (id);

ALTER TABLE mensagem
  ADD CONSTRAINT FK_usuario_TO_mensagem
    FOREIGN KEY (remetente_id)
    REFERENCES usuario (id);

ALTER TABLE conversa
  ADD CONSTRAINT FK_usuario_TO_conversa
    FOREIGN KEY (remetente_id)
    REFERENCES usuario (id);

ALTER TABLE conversa
  ADD CONSTRAINT FK_usuario_TO_conversa1
    FOREIGN KEY (destinatario_id)
    REFERENCES usuario (id);

ALTER TABLE mensagem
  ADD CONSTRAINT FK_conversa_TO_mensagem
    FOREIGN KEY (conversa_id)
    REFERENCES conversa (id);

ALTER TABLE mensagem
  ADD CONSTRAINT FK_status_mensagem_TO_mensagem
    FOREIGN KEY (status_id)
    REFERENCES status_mensagem (id);

ALTER TABLE conversa
  ADD CONSTRAINT FK_status_conversa_TO_conversa
    FOREIGN KEY (status_id)
    REFERENCES status_conversa (id);

ALTER TABLE item
  ADD CONSTRAINT FK_usuario_TO_item
    FOREIGN KEY (proprietario_id)
    REFERENCES usuario (id);

ALTER TABLE troca
  ADD CONSTRAINT FK_usuario_TO_troca
    FOREIGN KEY (usuario1_id)
    REFERENCES usuario (id);

ALTER TABLE troca
  ADD CONSTRAINT FK_usuario_TO_troca1
    FOREIGN KEY (usuario2_id)
    REFERENCES usuario (id);

ALTER TABLE troca
  ADD CONSTRAINT FK_item_TO_troca
    FOREIGN KEY (item1_id)
    REFERENCES item (id);

ALTER TABLE troca
  ADD CONSTRAINT FK_item_TO_troca1
    FOREIGN KEY (item2_id)
    REFERENCES item (id);

ALTER TABLE autor_item
  ADD CONSTRAINT FK_item_TO_autor_item
    FOREIGN KEY (item_id)
    REFERENCES item (id);

ALTER TABLE autor_item
  ADD CONSTRAINT FK_autor_TO_autor_item
    FOREIGN KEY (autor_id)
    REFERENCES autor (id);
