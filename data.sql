INSERT INTO genero(id, genero) 
VALUES 
    (1, 'Ficção'),
    (2, 'Comédia'),
    (3, 'Romance'),
    (4, 'Ação e Aventura'),
    (5, 'Horror'),
    (6, 'Suspense'),
    (7, 'Ficção Científica'),
    (8, 'Fantasia'),
    (9, 'Drama'),
    (10, 'Policial');
 
INSERT INTO status_conversa(id, status)
VALUES 
    (1, 'Aberta'),
    (2, 'Fechada');

INSERT INTO status_mensagem(id, status)
VALUES
    (1, 'Pendente'),
    (2, 'Enviada'),
    (3, 'Recebida'),
    (4, 'Lida');
